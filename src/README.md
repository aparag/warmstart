GOAL: Supervised learning of value function and optimal trajectory to warmstart the unicycle problem.

For all experiments of unicycle:

Horizon      = 20

cost_Weights = [1.5, 1]

Precision    = 1e-9                   --> Regular solver

Precision    = 1e-7                   --> When neural networks are used inside crocoddyl


# The first neural nets in SUbsampled with 500 starting points
# The seconds one is with 1000 starting points.