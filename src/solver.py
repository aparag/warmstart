"""
Given a starting position (terminal model and state trajectory guess), solve the unicycle problem and return the ddp solver 
"""
from typing import Union
import numpy as np
import crocoddyl

def optimalSolution(terminal_model = None, state_guess: Union[list, np.ndarray] = None, 
                    starting_position:Union[list, np.ndarray] = None, horizon:int = 30,
                    weights:list = [1.5, 1.], precision:float = 1e-9)-> crocoddyl.SolverDDP:
    """Solve a unicycle problem with the given parameters
    
    ARGS
    -------

    :param      terminal_model   : terminal_model to be used for the unicycle. 
                                   This can be either a terminal model with neural net inside it or a custom loss function inside it.
                                   See terminal_models.py for more details.
    
    :param      state_guess      : List of lists or arrays to be used as init_xs.
    
    :param      starting_position: List or array of starting position for the unicycle in cartesian coordinates.
    
    :param      horizon          : Horizon of the problem. Default set to 20.
    
    :param      weights          : List of state and cost weights for the unicycle problem. Default set to [1.5, 1].
                                   Crocoddyl will solve the problem for 
                                   model.costWeights = np.array([1.5, 1]), where 1.5 is the state weight and 1 is the control weight.
    
    :param      precision        : float precision for ddp.th_stop. When using terminal_model inside crocoddyl, set the precision to 
                                   1e-7. Default value is 1e-9
    
    RETURNS
    -------
    :return      ddp            : the ddp solution to the problem.

    """
    try:
        assert starting_position is not None
    except ValueError:
        print("No starting position provided..")

    starting_position = np.atleast_2d(starting_position)            
    assert starting_position.shape == (1, 3)                    # crocoddyl is jittery about the input shape of numpy arrays.

    model = crocoddyl.ActionModelUnicycle()                     # instantiate running action model
    model.costWeights = np.array([*weights]).T                  # model state and control weights

    # Set up the optimization problem
    if terminal_model is None:
        problem = crocoddyl.ShootingProblem(starting_position.T, [ model ] * horizon, model)
    else:
        problem = crocoddyl.ShootingProblem(starting_position.T, [ model ] * horizon, terminal_model)

    # Define ddp
    ddp = crocoddyl.SolverDDP(problem)
    # Define precision
    ddp.th_stop = precision

    # state guess
    # The state guess for state trajectory should either be a list of arrays or array of shape {horizon+1, 3}
    if state_guess is not None:
        state_guess = np.atleast_2d(state_guess)
        assert state_guess.shape == (horizon+1, 3)
    else:
        state_guess = []
    
    ddp.solve(state_guess,[], 1000)
    del terminal_model, model, starting_position
    return ddp



    




                                                   


