import numpy as np


def cartesian_points(n_points:int = 500):
    
    """Draw a collection of grid and random points of the given size
    The ratio of equidistant grid points and random points is 4:1
    i,e if n_points = 1500, then 1/4th of these points will be randomly generated 
    while the remaining 3/4th points are equidistant grid points.
    """
    
    try:
        n_points = np.abs(np.int(n_points))
        assert n_points > 0 

        if n_points >= 4 :
            n_grid_points = 3 *int(n_points / 4)
            grid_spacing  = int(np.cbrt(n_grid_points))
            xy_range = np.linspace(-2,2,num = grid_spacing, endpoint=True)
            theta_range = np.linspace(-np.pi, np.pi, num=grid_spacing, endpoint=True)
            
            grid = np.array([[x, y, z] for x in xy_range for y in xy_range for z in theta_range])
            n_random_points = int(n_points - grid.shape[0])
            
            random_points = []
            for _ in range(n_random_points):
                point = [np.random.uniform(-2., 2), np.random.uniform(-2., 2), np.random.uniform(-np.pi, np.pi)]
                random_points.append(point)
            
            random_points = np.array(random_points)
            
            data = np.vstack((grid, random_points))
            np.random.shuffle(data)
            del random_points, grid
            return np.array(data)
        elif n_points >=1 and n_points <= 3:
            data = []
            for _ in range(n_points):
                point = [np.random.uniform(-2., 2), np.random.uniform(-2., 2), np.random.uniform(-np.pi, np.pi)]
                data.append(point)            
            data = np.array(data)
            return np.array(data)
    except ValueError:
        print("No idea")


def cartesian_grid(n_points:int = 500):
    
    """Draw a collection of grid and random points of the given size
    The ratio of equidistant grid points and random points is 4:1
    i,e if n_points = 1500, then 1/4th of these points will be randomly generated 
    while the remaining 3/4th points are equidistant grid points.
    """
    
    try:
        n_points = np.abs(np.int(n_points))
        assert n_points > 0 

        
        grid_spacing  = int(np.cbrt(n_points))
        xy_range = np.linspace(-2,2,num = grid_spacing, endpoint=True)
        theta_range = np.linspace(-np.pi, np.pi, num=grid_spacing, endpoint=True)
            
        grid = np.array([[x, y, z] for x in xy_range for y in xy_range for z in theta_range])
           
   
       
        np.random.shuffle(grid)
        return np.array(grid)
    except ValueError:
        print("No idea")

