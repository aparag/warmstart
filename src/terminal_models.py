"""
Terminal models for the unicycle problem.
Two kinds of terminal models are implemeted.
    1: A terminal model with custom loss function
    2: A terminal model with neural network as input to calculate cost-to-go.

"""
import numpy as np
import torch
from value_approximator import ValueApproximator
torch.set_default_dtype(torch.float32)

import torch
import numpy as np
import crocoddyl
torch.set_default_dtype(torch.float32)



class ValueModelUnicycle(crocoddyl.ActionModelAbstract):
    """
    This includes a feedforward network in crocoddyl
    
    """
    def __init__(self, neural_net):
        crocoddyl.ActionModelAbstract.__init__(self, crocoddyl.StateVector(3), 2, 5)
        self.net = neural_net

    def calc(self, data, x, u=None):
        if u is None:
            u = self.unone
            
        x = torch.Tensor(x).resize_(1, 3)
        
        # Get the cost
        with torch.no_grad():
            data.cost = self.net(x).item()


    def calcDiff(self, data, x, u=None):
        if u is None:
            u = self.unone
            
        # This is irritating. Converting numpy to torch everytime.
        x = torch.Tensor(x).resize_(1, 3)

        j = np.array(self.net.gradient(x))
        h = np.array(self.net.hessian(x))
        data.Lx = j
        data.Lxx = h

class LossModelUnicycle(crocoddyl.ActionModelAbstract):
    """Implement a custom loss function to be used as terminal loss in unicycle"""
    #TODO
    pass