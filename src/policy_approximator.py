"""This module implements a pytorch feedforward network to learn state policy.

Training of this network is slightly tricky because it optimzes variable length dataset when subsampled IREPA is used.

The default architecture is a 4-layered neural network with ReLU activation function.

"""
import numpy as np
import torch
import torch.nn as nn
torch.set_default_dtype(torch.float32)


class StatePolicyApproximator(nn.Module):
    def __init__(self, input_dimensions:int = 3, max_horizon:int = 20,
                hidden_units:list = [32, 32, 32], activation = nn.ReLU()):



        """Instantiate an untrained neural network with the given params
        
        Args
        ........
                
        :param      input_dimensions         = dimensions of q of the robot. state space  = [q, qdot].T 
                                                    q = [x, y, theta] for unicycle

        :param      max_horizon              = length of the horizon. Output dimension is 3 * max_horizon.

        :param      fc1_dims                 = number of units in the first fully connected layer. Default 32
        
        :param      fc2_dims                 = number of units in the second fully connected layer. Default 32

        :param      fc3_dims                 = number of units in the third fully connected layer. Default 32
        
        :param      activation               = activation for the layers, default ReLU().
    
        """
        super(StatePolicyApproximator, self).__init__()


        fc1_dims, fc2_dims, fc3_dims = hidden_units



        self.input_dimensions   = input_dimensions
        self.output_dimensions  = 3 * max_horizon
        self.fc1_dims           = fc1_dims
        self.fc2_dims           = fc2_dims
        self.fc3_dims           = fc3_dims
        self.activation_hidden  = activation

        # This structure seems to work.
        #........... Structure
        self.fc1 = nn.Linear(self.input_dimensions, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
        self.fc3 = nn.Linear(self.fc2_dims, self.fc3_dims)
        self.fc4 = nn.Linear(self.fc3_dims, self.output_dimensions)



        #........... Weight Initialization
        nn.init.kaiming_normal_(self.fc1.weight)
        nn.init.kaiming_normal_(self.fc2.weight)
        nn.init.kaiming_normal_(self.fc3.weight)
        nn.init.kaiming_normal_(self.fc4.weight)

        #........... Bias Initialization
        nn.init.constant_(self.fc1.bias, 0.001)
        nn.init.constant_(self.fc2.bias, 0.001)
        nn.init.constant_(self.fc3.bias, 0.001)
        nn.init.constant_(self.fc4.bias, 0.001)




    def forward(self, state):
        """
        The state trajectory learned by the neural network.
        
        """
        state_policy = self.activation_hidden(self.fc1(state))
        state_policy = self.activation_hidden(self.fc2(state_policy))
        state_policy = self.activation_hidden(self.fc3(state_policy))
        state_policy = self.fc4(state_policy)
        return state_policy

    @property
    def policy_shape(self):
        policy_length = int(self.output_dimensions)
        policy_shape0 = int(policy_length/3)
        return [policy_shape0, 3]

    def warmstart_policy(self, state):
        """
        Returns the guess for warmstarting in the format required by crocoddyl
        """
        init_xs = []
        init_xs.append(state.cpu().detach().numpy())
        init_xs = np.array(init_xs).reshape(1,3)
        shape = self.policy_shape
        with torch.no_grad():
            rollout = self.forward(state).cpu().detach().numpy().squeeze().reshape(*shape)
        init_xs = np.vstack((init_xs, rollout))
        return np.array(init_xs)