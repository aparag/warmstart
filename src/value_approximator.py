
import numpy as np
import torch
import torch.nn as nn
import crocoddyl
import torch.nn.functional as F



torch.set_default_dtype(torch.float32)

class ValueApproximator(nn.Module):
    """
    A simple feed forward network to implement value approximation   
    
    """
    def __init__(self, input_dimensions:int = 3, output_dimensions:int = 1,
                hidden_units:list = [32, 32], activation = nn.Tanh()):

        
        """Instantiate an untrained neural network with the given params
        
        Args
        ........
                
        :param      input_dimensions         = int ,3. Dimensions of q of the robot. state space  = [q, qdot].T 
                                                        q = [x, y, theta] for unicycle

        :param      output_dimensions        = int, 1. Dimensions of the value function

        :param      fc1_dims                 = int. Number of units in the first fully connected layer. Default 32
        
        :param      fc2_dims                 = int. Number of units in the second fully connected layer. Default 32

        :param      fc3_dims                 = int. Number of units in the third fully connected layer. Default 12
        
        :param      activation               = Activation for the layers, default Tanh().
    
        """

        super(ValueApproximator, self).__init__()

        fc1_dims, fc2_dims      = hidden_units


        self.input_dimensions   = input_dimensions
        self.output_dimensions  = output_dimensions
        self.fc1_dims           = fc1_dims
        self.fc2_dims           = fc2_dims
        self.activation         = activation

        #........... Structure. 4 fully connected layers
        self.fc1 = nn.Linear(self.input_dimensions, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)
        self.fc3 = nn.Linear(self.fc2_dims, self.output_dimensions)

        #........... Weight Initialization
        nn.init.kaiming_normal_(self.fc1.weight)
        nn.init.kaiming_normal_(self.fc2.weight)
        nn.init.kaiming_normal_(self.fc3.weight)
       


        #........... Bias Initialization
        nn.init.constant_(self.fc1.bias, 0.01)
        nn.init.constant_(self.fc2.bias, 0.01)
        nn.init.constant_(self.fc3.bias, 0.01)



    def forward(self, state):
        """
        The state here is the cartesian coordinates of the position corresponding to the value function.
        
        """
        
        value = self.activation(self.fc1(state))
        value = self.activation(self.fc2(value))
        value = self.fc3(value)
        return value


    def gradient(self, state):
        """
        Args
        ......
        :param       x      = q. Cartesian coordinates of position, where q = [x, y, theta]
        
        Returns
        .......
        :return      j      = jacobian, where the jacobian is the gradient of the Value function with respect to q.
                                Jacobian = dV/dq
        
        """
        return torch.autograd.functional.jacobian(self.forward, state).cpu().detach().numpy().squeeze()


    def hessian(self, state):
        """
        Args
        ......

        :param       x      = q. Cartesian coordinates of position, where q = [x, y, theta]
        
        
        Returns
        .......
        
        :return     h       =  hessian,  
                              h is the true hessian of the Value function with respect to q, if the activation function is Tanh().
                              else h = Gauss-Newton Hessian        
                
                              Gauss-Newton Hessian = J.T @ J, this approximation works only when y_pred - y_true ~ = 0.
        """
        # if activation is not tanh, then calculate the Gauss Approximation of the Hessian.
        if not isinstance(self.activation, torch.nn.modules.activation.Tanh):           
            j = self.gradient(state).reshape(1, 3)
            h = j.T @ j                                     # Gauss Approximation. 
            return h
        else:# return the true hessian
            return torch.autograd.functional.hessian(self.forward, state).cpu().detach().numpy().squeeze()



    def batch_gradient(self, states):
        """
        Returns the jacobians of multiple inputs. This is going to be used in Sobolev Training.
        """
        j = [self.gradient(state) for state in states]

        return np.array(j).squeeze()

    def batch_hessian(self, states):
        """
        Returns the hessians of the multiple inputs. This is going to be used in Sobolev Training.
        
        """
        h = [self.hessian(state) for state in states]
        print(h)
        return np.array(h)

