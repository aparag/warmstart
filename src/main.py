"""
IREPA RUNS = 30

In this experiment, value approximators and trajectory approximators are trained on a subsampled dataset.

The training process:

1: Generate 500 starting cartesian points.
2: Use crocoddyl to generate 500 trajectories.
3: Subsample the 500 trajectories to create starting datasets for value approximator and policy approximator.
4: Train the neural networks
5: Repeat until Convergence:
    1: Repeat steps 1 --> 4.
    2: In step 2, use terminal_value_approximator to generate trajectories. 


"""

import numpy as np
import torch
import crocoddyl
from policy_approximator import StatePolicyApproximator
from value_approximator import ValueApproximator
from solver import optimalSolution
from terminal_models import ValueModelUnicycle
from utils import cartesian_points
from dataset import Datagen
torch.set_default_dtype(torch.float32)

class IREPA():

    def __init__(self):
        """
        define the hyperparams for the experiments.       
        """
        #... Crocoddyl hyperparams
        self.HORIZON                          = 20
        self.WEIGHTS                          = [1.5, 1]
        self.PRECISION                        = 1e-6

        #... Value Approximator
        self.valueApproximator_nhiddenunits  = [256, 256]              # 2 hidden layers [60, 30, 15 combo works]
        self.valueApproximator_activation    = torch.nn.Tanh()        # activation
        
        #... Policy Approximator
        self.policyApproximator_nhiddenunits = [32, 32, 32]            # 3 hidden layers [32, 32, 32 combo works]
        self.policyApproximator_activation   = torch.nn.ReLU()         # with Relu activation
        

        #... Global Training hyperparams  
        self.n_trajectories                       = 700
        self.subsample                            = True
        self.learning_rate                        = 5e-3               # Params for Adam optimizer    
        self.optimizer_decay                      = 1e-6
        self.batch_size                           = 64
        self.iterations                           = 2                  # Number of iterations for IREPA
        self.epochs                               = 2000               # Number of epochs in each iteration

        if self.subsample:
            self.save_path = "./neural_networks/subsampled/"
        else:
            self.save_path = "./neural_networks/sampled/"

    def instantiateValueApproximator(self):
        """Instantiate a network to learn value"""
        value_net = ValueApproximator(hidden_units=self.valueApproximator_nhiddenunits,
                                      activation=self.valueApproximator_activation)
        return value_net


    def instantiateStatePolicyApproximator(self):
        """Instantiate a network to learn policy"""
        state_net = StatePolicyApproximator(max_horizon=self.HORIZON,
                                            hidden_units=self.policyApproximator_nhiddenunits,
                                            activation=self.policyApproximator_activation)
        return state_net

    def getTrainingData(self, terminal_unicycle=None):
        """Returns the dataloader for pytorch training"""

        states, values, trajectories = Datagen.statesValuesTrajectories(terminal_model=terminal_unicycle,
                                                                        n_trajectories=self.n_trajectories,
                                                                        horizon=self.HORIZON,
                                                                        weights=self.WEIGHTS,
                                                                        precision=self.PRECISION,
                                                                        subsample=self.subsample)
        
        states              = torch.Tensor(states)
        values              = torch.Tensor(values)
        trajectories        = torch.Tensor(trajectories)
        
        v_datas             = torch.utils.data.TensorDataset(states, values)
        value_data_loader   = torch.utils.data.DataLoader(v_datas, batch_size = self.batch_size, shuffle=True)

        p_datas             = torch.utils.data.TensorDataset(states, trajectories)
        policy_data_loader  = torch.utils.data.DataLoader(p_datas, batch_size = self.batch_size, shuffle=True)
        print(states.shape[0])
        return value_data_loader, policy_data_loader

    def _train(self, value_net, policy_net, value_data_loader, policy_data_loader):

        # Define Loss and optimizer for value_network
        value_net_mse_loss  = torch.nn.MSELoss()
        value_net_optimizer = torch.optim.Adam(value_net.parameters(), lr = self.learning_rate,
                                               weight_decay=self.optimizer_decay)
        value_net.train()

        # Define loss and optimizer for policy_network
        policy_net_mse_loss  = torch.nn.MSELoss()
        policy_net_optimizer = torch.optim.Adam(policy_net.parameters(), lr = self.learning_rate,
                                               weight_decay=self.optimizer_decay)
        policy_net.train()
        if not self.subsample:
            # for training of unsubsampled dataset:
            for i in range(self.epochs):
                # Train the value network
                i += 1
                running_value_loss = []
                for data, target in value_data_loader:

                    output           = value_net(data)
                    value_net_loss   = value_net_mse_loss(output, target) 
                    
                    value_net_optimizer.zero_grad()
                    value_net_loss.backward()
                    value_net_optimizer.step()
                    running_value_loss.append(value_net_loss)

                running_policy_loss = []
                for data, target in policy_data_loader:

                    output           = policy_net(data)
                    policy_net_loss   = policy_net_mse_loss(output, target)
                    
                    policy_net_optimizer.zero_grad()
                    policy_net_loss.backward()
                    policy_net_optimizer.step()
                    running_policy_loss.append(policy_net_loss)
                if i % 10 == 1:
                    print(f'Value Network :   Epoch  : {i:3}  Loss   : {value_net_loss.item():10.8f}')
                    print(f'Policy Network :   Epoch  : {i:3}  Loss   : {policy_net_loss.item():10.8f}')
                    print("\n")
        else:
            # for training of subsampled dataset:
            for i in range(self.epochs):
                # Train the value network
                i += 1
                running_value_loss = []
                for data, target in value_data_loader:

                    output           = value_net(data[:,:-1])
                    value_net_loss   = value_net_mse_loss(output, target) 
                    
                    value_net_optimizer.zero_grad()
                    value_net_loss.backward()
                    value_net_optimizer.step()
                    running_value_loss.append(value_net_loss)

                running_policy_loss = []
                for data, target in policy_data_loader:
                    
                    
                    loss = 0.
                    
                    for state, traj in zip(data, target):
                        sub_trajectory_length = int(state[-1].item())
                        output                = policy_net(state[:-1])

                        true_trajectory       = traj[:sub_trajectory_length]
                        pred_trajectory       = output[:sub_trajectory_length]

                        assert len(true_trajectory) == len(pred_trajectory) == sub_trajectory_length

                        error = policy_net_mse_loss(pred_trajectory, true_trajectory)
                        loss = loss + error
                    
                    policy_net_optimizer.zero_grad()
                    policy_net_loss = loss/data.shape[0]
                    policy_net_loss.backward()
                    policy_net_optimizer.step()
                    running_policy_loss.append(policy_net_loss)

                if i % 10 == 1:
                    print(f'Value Network :   Epoch  : {i:3}  Loss   : {value_net_loss.item():10.8f}')
                    print(f'Policy Network :   Epoch  : {i:3}  Loss   : {policy_net_loss.item():10.8f}')
                    print("\n")

        
        del value_data_loader, policy_data_loader, policy_net_optimizer, value_net_optimizer, value_net_loss, policy_net_loss, \
            running_policy_loss, running_value_loss
        
        return value_net, policy_net







    def train(self):
        """MAIN IREPA LOOP"""

        # Instantiate the value and policy approximators
        value_net  = self.instantiateValueApproximator()
        policy_net = self.instantiateStatePolicyApproximator()
        print(value_net)
        print("\n\n")
        print(policy_net)


        for _ in range(self.iterations):
            print(f"####################### IREPA {_} ##############################\n")
            # In the first irepa run, Crocoddyl will not use a custom terminal model.
            if _ == 0:
                value_data_loader, policy_data_loader = self.getTrainingData()
                
                value_net, policy_net = self._train(value_net, policy_net,
                                                    value_data_loader, policy_data_loader)
            else:
                terminal_model_unicycle = ValueModelUnicycle(value_net)
                
                value_data_loader, policy_data_loader = self.getTrainingData(terminal_unicycle=terminal_model_unicycle)
                
                value_net, policy_net = self._train(value_net, policy_net,
                                                    value_data_loader, policy_data_loader)

            torch.save(value_net, self.save_path+f"value_net{_+1}.pth")
            torch.save(policy_net, self.save_path+f"policy_net{_+1}.pth")


            # Validate approximators after each IREPA iteration
            #self.validateApproximators(value_net, policy_net)


        

    #def validateApproximators(value_net, policy_net):
    #    pass

    


if __name__=='__main__':
    irepa = IREPA()
    irepa.train()    






    