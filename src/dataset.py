"""
Dataset module does the heavy lifitng of generating data for training and validation.
"""

import numpy as np
import torch
import crocoddyl
import random
from solver import optimalSolution
from utils import cartesian_points
from utils import cartesian_grid



torch.set_default_dtype(torch.float32)

class Datagen:
    def statesValuesTrajectories(terminal_model = None, n_trajectories:int = 500,
                                horizon:int = 20, weights:list = [1.5, 1],
                                precision:float = 1e-6, subsample:bool = False):
        """After generating the given number of trajectories to generate, return starting states, values 
        and trajectories.
        

        ARGS
        ------

        :param      terminal_model      = terminal_model to be used inside crocoddyl.If none, then ActionModelUnicycle() is used.
        
        :param      n_trajectories      = int. The number of trajectories to generate for training.
                                            For IREPA without subsampling the number should be > 1000. 
                                            For IREPA with subsampling, the minimum number should be > 400.
        :param      horizon             = int. Length of the horizon to solve the crocoddyl optimation problem.
                                            Default 20 for unicycle.
        
        :param      weights             = state and control weights for the problems.
        
        :param      precision           = float, ddp.th_stop.

        :param      subsampled          = bool, False. If true, return the subsampled trajectories.
             
        """
        # Get n starting points in 3D space
        initial_start_configurations = cartesian_grid(n_points = n_trajectories)
        initial_start_configurations = np.atleast_2d(initial_start_configurations)
        # The training data containers for the two value and policy approximators.
        states          = []
        values          = []
        trajectories    = []
        print("Getting dataset...")
        if subsample:
            for position in initial_start_configurations:
                # Get the ddp solution
                ddp = optimalSolution(terminal_model=terminal_model, state_guess=None,
                                    starting_position=position, horizon=horizon,
                                    weights=weights,precision=precision)

                # Each trajectory will generate multiple datapoints. The terminal position is not included in the dataset
                # since it has no subtrajectories.
                xs = np.array(ddp.xs)
                # Cost-to-go for every node(but last) in the horizon for the particular problem
                cost_to_go = []
                for d in ddp.problem.runningDatas:
                    cost_to_go.append(d.cost)
                    
                for i in range(len(cost_to_go)):
                    cost_to_go[i] =  sum(cost_to_go[i:]) + ddp.problem.terminalData.cost
                
                assert len(cost_to_go) == horizon
                
                # So we have xs and cost_to_go
                for index, node_cost in enumerate(zip(xs, cost_to_go)):
                    # length of subtrajectory from the starting_state. Should be equal to Horizon length.
                    length_subtrajectories = len(xs[index+1:])
                    node , cost = node_cost
                    current_state = [i for i in node]
                    current_state.extend([length_subtrajectories*3])
                    subtraj       = xs[index+1:].flatten().tolist()
                    while len(subtraj) != 3*horizon:
                        subtraj.append(0)
                    states.append(current_state)
                    values.append([cost])
                    trajectories.append(subtraj)

        
        else:
            for position in initial_start_configurations:
                # Get the ddp solution
                ddp = optimalSolution(terminal_model=terminal_model, state_guess=None,
                                    starting_position=position, horizon=horizon,
                                    weights=weights,precision=precision)
                
                # The state trajectories is from ddp.xs[1:]
                xs = np.array(ddp.xs[1:])
                states.append(position)
                values.append([ddp.cost])
                trajectories.append(xs.flatten())





        # Shuffle the dataset
        dataset = list(zip(states, values, trajectories))
        random.shuffle(dataset)
        states, values, trajectories = zip(*dataset)
        
        states = np.array(states)
        values = np.array(values).reshape(-1, 1)
        trajectories = np.array(trajectories)

        return states, values, trajectories

    








