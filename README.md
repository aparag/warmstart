Supervised learning of value function and optimal trajectory to warmstart the [Crocoddyl](https://github.com/loco-3d/crocoddyl).

Value approximator and Policy approximators iteratively learn the optimal value and policy from an offline database and are then used to warmstart the solver in Crocoddyl.

See [IREPA](https://hal.archives-ouvertes.fr/hal-01591373/document) for more details.


There are a few experiments in this folder:

1: IREPA without subsampling.

    1: Offline database of 10000 trajectories are used to learn value function and optimal trajectory iteratively 

2: IREPA with subsampling.

    1: A database of size (500 * Horizon) is used to learn value function and optimal trajectory.

3: Sobolev Training.

    TODO

